## README

`C:\_\daw\.assets\fonts`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `33`
- `thought_uid` - `DAE9054B-ED4F-D82E-B3E7-C13042E083A3`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/GGMKFNqGAqAAAhUF`
- `FmColorName` - `blue-dark`
- `ColorHex` - `#455DAC`
- `ColorRgb` - `69,93,172`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-dark-48.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cfonts) _Font Assets_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-white-light-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%32%37%26webthought_id%3D%32%37%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2Fd%36Pr%35NqGAqAAAgzf) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cfonts%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FGGMKFNqGAqAAAhUF) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FGGMKFNqGAqAAAhUF) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/DAE%39%30%35%34B-ED%34F-D%38%32E-B%33E%37-C%31%33%30%34%32E%30%38%33A%33) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/.assets/fonts/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-33) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cfonts%5Cunused%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%33%34%26webthought_id%3D%33%34%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FZTC%36FNqGAqAAAhXx)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cfonts%5Cunused%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%33%34%26webthought_id%3D%33%34%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FZTC%36FNqGAqAAAhXx) `unused` - Unused Font Assets

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cfonts%5CREADME.html) `README.html`

