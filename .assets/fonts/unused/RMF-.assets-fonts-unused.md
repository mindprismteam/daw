## RMF-.assets-fonts-unused

`C:\_\daw\.assets\fonts\unused`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `34`
- `thought_uid` - `21D983FF-0E44-DF5B-AABF-EF359FD73A25`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/ZTC6FNqGAqAAAhXx`
- `FmColorName` - `blue`
- `ColorHex` - `#6C7FD4`
- `ColorRgb` - `108,127,212`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-48.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cfonts%5Cunused) _Unused Font Assets_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cfonts%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%33%33%26webthought_id%3D%33%33%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FGGMKFNqGAqAAAhUF) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cfonts%5Cunused%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FZTC%36FNqGAqAAAhXx) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FZTC%36FNqGAqAAAhXx) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%32%31D%39%38%33FF-%30E%34%34-DF%35B-AABF-EF%33%35%39FD%37%33A%32%35) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/.assets/fonts/unused/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-34) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cfonts%5Cunused%5Cfont-awesome-%34.%37.%30.zip) `font-awesome-4.7.0.zip`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cfonts%5Cunused%5CREADME.html) `README.html`

