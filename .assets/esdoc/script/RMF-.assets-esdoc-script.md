## RMF-.assets-esdoc-script

`C:\_\daw\.assets\esdoc\script`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `31`
- `thought_uid` - `4F2EAADD-CA13-5C47-CB22-B5B58A1E3D47`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/JzrqFNqGAqAAAhWD`
- `FmColorName` - `black`
- `ColorHex` - `#525257`
- `ColorRgb` - `82,82,87`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-48.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cesdoc%5Cscript) _Source EsDoc built-in scripts, and additional scripts plus `manual.js` custom script_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cesdoc%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%32%39%26webthought_id%3D%32%39%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FooEKFNqGAqAAAhT%35) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cesdoc%5Cscript%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FJzrqFNqGAqAAAhWD) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FJzrqFNqGAqAAAhWD) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%34F%32EAADD-CA%31%33-%35C%34%37-CB%32%32-B%35B%35%38A%31E%33D%34%37) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/.assets/esdoc/script/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-31) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cesdoc%5Cscript%5Cprettify%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%33%32%26webthought_id%3D%33%32%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2Fcm.aFNqGAqAAAhW%32)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cesdoc%5Cscript%5Cprettify%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%33%32%26webthought_id%3D%33%32%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2Fcm.aFNqGAqAAAhW%32) `prettify` - Source Prettify Code

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cesdoc%5Cscript%5Cclipboard.min.js) `clipboard.min.js` - Clipboard Copy
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cesdoc%5Cscript%5CcontextMenu.min.js) `contextMenu.min.js` - Context Menu
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cesdoc%5Cscript%5Cjquery-%32.%31.%34.min.js) `jquery-2.1.4.min.js` - EsDocs wants
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cesdoc%5Cscript%5Cjquery.qtip.min.js) `jquery.qtip.min.js` - QTip2 for jQuery
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cesdoc%5Cscript%5CloadCSS.js) `loadCSS.js` - Load CSS file asynch with window.loadCSS(href, before, media, idd)`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cesdoc%5Cscript%5Clscache.min.js) `lscache.min.js` - localStorage extension [lscashe Github](https://github.com/pamelafox/lscache)
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cesdoc%5Cscript%5Cmanual.js) `manual.js` - Mark Robbins EsDoc Enhancements `window._`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cesdoc%5Cscript%5Cnotify.min.js) `notify.min.js` - [notifyjs.com](https://notifyjs.com/)
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cesdoc%5Cscript%5ConloadCSS.js) `onloadCSS.js` - adds onload support for asynchronous stylesheets loaded with `loadCSS.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cesdoc%5Cscript%5CREADME.html) `README.html`

