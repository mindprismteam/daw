## README

`C:\_\daw\.assets`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `27`
- `thought_uid` - `D1EBF941-D09A-E120-28A4-C975B9F5A571`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/d6Pr5NqGAqAAAgzf`
- `FmColorName` - `white-light`
- `ColorHex` - `#FDFDFD`
- `ColorRgb` - `253,253,253`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-white-light-48.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets) _Various Assets for tools and site_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%32%26webthought_id%3D%32%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FE%31%36L%35NqGAqAAAgwr) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2Fd%36Pr%35NqGAqAAAgzf) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2Fd%36Pr%35NqGAqAAAgzf) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/D%31EBF%39%34%31-D%30%39A-E%31%32%30-%32%38A%34-C%39%37%35B%39F%35A%35%37%31) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/.assets/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-27) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

- `< more-types>`

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cesdoc%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%32%39%26webthought_id%3D%32%39%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FooEKFNqGAqAAAhT%35)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cesdoc%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%32%39%26webthought_id%3D%32%39%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FooEKFNqGAqAAAhT%35) `esdoc` - EsDoc enhancements, css and script
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cfonts%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%33%33%26webthought_id%3D%33%33%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FGGMKFNqGAqAAAhUF)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cfonts%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%33%33%26webthought_id%3D%33%33%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FGGMKFNqGAqAAAhUF) `fonts` - Font Assets
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cimages%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%33%35%26webthought_id%3D%33%35%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FpXCKFNqGAqAAAhUP)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cimages%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%33%35%26webthought_id%3D%33%35%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FpXCKFNqGAqAAAhUP) `images` - Image Assets, Originals
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cnode_modules%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%33%36%26webthought_id%3D%33%36%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FLDKKFNqGAqAAAhUZ)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cnode_modules%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%33%36%26webthought_id%3D%33%36%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FLDKKFNqGAqAAAhUZ) `node_modules` - see `/_tools/reset-nm.bat` :: Dummy Directory for preserving Icon
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%33%37%26webthought_id%3D%33%37%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FQdmKFNqGAqAAAhUj)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%33%37%26webthought_id%3D%33%37%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FQdmKFNqGAqAAAhUj) `plato` - Plato Publish Overrides
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%34%37%26webthought_id%3D%34%37%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FfaeKFNqGAqAAAhUt)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%34%37%26webthought_id%3D%34%37%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FfaeKFNqGAqAAAhUt) `visual_paradigm_publish` - Visual Paradigm Publish Overlay Assets
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5C_crushed%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%32%38%26webthought_id%3D%32%38%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2Fg%38wKFNqGAqAAAhTt)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5C_crushed%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%32%38%26webthought_id%3D%32%38%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2Fg%38wKFNqGAqAAAhTt) `_crushed` - temp crush folder :: Temp Crushed Images

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5CREADME.html) `README.html`

