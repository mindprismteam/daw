## RMF-.assets-plato-assets-scripts-vendor-codemirror

`C:\_\daw\.assets\plato\assets\scripts\vendor\codemirror`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `45`
- `thought_uid` - `40B0FC2E-F5C9-6274-29E1-E0042F6D7FDB`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/NXNWFNqGAqAAAhbh`
- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror) _Source Plato Codemirror Library Scripts_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%34%34%26webthought_id%3D%34%34%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FOC.WFNqGAqAAAhbC) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FNXNWFNqGAqAAAhbh) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FNXNWFNqGAqAAAhbh) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%34%30B%30FC%32E-F%35C%39-%36%32%37%34-%32%39E%31-E%30%30%34%32F%36D%37FDB) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/.assets/plato/assets/scripts/vendor/codemirror/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-45) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/daw/esdoc/file/.assets/plato/assets/scripts/vendor/codemirror/codemirror.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/daw/plato/files/plato_assets_scripts_vendor_codemirror_codemirror_js/index.html) - plato
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%34%36%26webthought_id%3D%34%36%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FnmHWFNqGAqAAAhb%31)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%34%36%26webthought_id%3D%34%36%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FnmHWFNqGAqAAAhb%31) `util` - Source Plato Codemirror Library Script Utils

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Ccodemirror.js) `codemirror.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cjavascript.js) `javascript.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5CREADME.html) `README.html`

