## RMF-.assets-plato-assets-scripts-vendor-codemirror-util

`C:\_\daw\.assets\plato\assets\scripts\vendor\codemirror\util`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `46`
- `thought_uid` - `3F562E9A-76A6-9CAA-7992-4E1970A50A5D`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/nmHWFNqGAqAAAhb1`
- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil) _Source Plato Codemirror Library Script Utils_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%34%35%26webthought_id%3D%34%35%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FNXNWFNqGAqAAAhbh) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FnmHWFNqGAqAAAhb%31) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FnmHWFNqGAqAAAhb%31) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%33F%35%36%32E%39A-%37%36A%36-%39CAA-%37%39%39%32-%34E%31%39%37%30A%35%30A%35D) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/.assets/plato/assets/scripts/vendor/codemirror/util/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-46) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cclosetag.js) `closetag.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Ccolorize.js) `colorize.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Ccontinuecomment.js) `continuecomment.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Ccontinuelist.js) `continuelist.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cdialog.css) `dialog.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cdialog.js) `dialog.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cfoldcode.js) `foldcode.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cformatting.js) `formatting.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cjavascript-hint.js) `javascript-hint.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cloadmode.js) `loadmode.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cmatch-highlighter.js) `match-highlighter.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cmatchbrackets.js) `matchbrackets.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cmultiplex.js) `multiplex.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Coverlay.js) `overlay.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cpig-hint.js) `pig-hint.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Crunmode-standalone.js) `runmode-standalone.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Crunmode.js) `runmode.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Csearch.js) `search.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Csearchcursor.js) `searchcursor.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Csimple-hint.css) `simple-hint.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Csimple-hint.js) `simple-hint.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cxml-hint.js) `xml-hint.js`

