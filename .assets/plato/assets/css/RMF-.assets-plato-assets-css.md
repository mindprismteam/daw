## RMF-.assets-plato-assets-css

`C:\_\daw\.assets\plato\assets\css`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `39`
- `thought_uid` - `D8BE0B0C-98B6-F038-27F9-04A892A15033`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/cTzGFNqGAqAAAhZJ`
- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Ccss) _Source Plato Styles_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%33%38%26webthought_id%3D%33%38%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FCpKGFNqGAqAAAhY%32) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Ccss%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FcTzGFNqGAqAAAhZJ) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FcTzGFNqGAqAAAhZJ) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/D%38BE%30B%30C-%39%38B%36-F%30%33%38-%32%37F%39-%30%34A%38%39%32A%31%35%30%33%33) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/.assets/plato/assets/css/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-39) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Ccss%5Cvendor%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%34%30%26webthought_id%3D%34%30%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FYFfGFNqGAqAAAhZW)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Ccss%5Cvendor%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%34%30%26webthought_id%3D%34%30%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FYFfGFNqGAqAAAhZW) `vendor` - Source Plato Vender Styles

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Ccss%5Cmorris.css) `morris.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Ccss%5Cplato-display.css) `plato-display.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Ccss%5Cplato-file.css) `plato-file.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Ccss%5Cplato-overview.css) `plato-overview.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Ccss%5Cplato.css) `plato.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5Ccss%5CREADME.html) `README.html`

