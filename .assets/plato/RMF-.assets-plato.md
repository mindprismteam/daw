## RMF-.assets-plato

`C:\_\daw\.assets\plato`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `37`
- `thought_uid` - `F341F092-9711-B48D-3456-4E669B1C5AFC`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/QdmKFNqGAqAAAhUj`
- `FmColorName` - `red-dark`
- `ColorHex` - `#F43F2A`
- `ColorRgb` - `244,63,42`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-48.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cplato) _Plato Publish Overrides_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-white-light-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%32%37%26webthought_id%3D%32%37%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2Fd%36Pr%35NqGAqAAAgzf) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FQdmKFNqGAqAAAhUj) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FQdmKFNqGAqAAAhUj) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/F%33%34%31F%30%39%32-%39%37%31%31-B%34%38D-%33%34%35%36-%34E%36%36%39B%31C%35AFC) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/.assets/plato/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-37) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%33%38%26webthought_id%3D%33%38%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FCpKGFNqGAqAAAhY%32)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5Cassets%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%33%38%26webthought_id%3D%33%38%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FCpKGFNqGAqAAAhY%32) `assets` - Source Plato styles, fonts and scripts

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cplato%5CREADME.html) `README.html`

