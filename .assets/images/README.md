## README

`C:\_\daw\.assets\images`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `35`
- `thought_uid` - `56210BC8-B3E4-0171-F441-277AAD2CE08A`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/pXCKFNqGAqAAAhUP`
- `FmColorName` - `green-dark`
- `ColorHex` - `#40C040`
- `ColorRgb` - `64,192,64`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-dark-48.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cimages) _Image Assets, Originals_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-white-light-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%32%37%26webthought_id%3D%32%37%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2Fd%36Pr%35NqGAqAAAgzf) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cimages%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FpXCKFNqGAqAAAhUP) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FpXCKFNqGAqAAAhUP) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%35%36%32%31%30BC%38-B%33E%34-%30%31%37%31-F%34%34%31-%32%37%37AAD%32CE%30%38A) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/.assets/images/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-35) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cimages%5Cbrain-logo-%32%32%38x%32%32%38.png) `brain-logo-228x228.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cimages%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cimages%5Cunnamed+-+Copy.png) `unnamed - Copy.png`

