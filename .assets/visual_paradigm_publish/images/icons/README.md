## README

`C:\_\daw\.assets\visual_paradigm_publish\images\icons`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `50`
- `thought_uid` - `82B117A4-E1F1-50B0-FC89-2A3C2264092D`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/9vn2FNqGAqAAAhc6`
- `FmColorName` - `black`
- `ColorHex` - `#525257`
- `ColorRgb` - `82,82,87`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-48.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons) _Visual Paradigm Publish Overlay Assets Images Directory_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%34%39%26webthought_id%3D%34%39%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2F%30su%32FNqGAqAAAhca) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2F%39vn%32FNqGAqAAAhc%36) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2F%39vn%32FNqGAqAAAhc%36) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%38%32B%31%31%37A%34-E%31F%31-%35%30B%30-FC%38%39-%32A%33C%32%32%36%34%30%39%32D) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/.assets/visual_paradigm_publish/images/icons/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-50) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5C-Ref.gif) `-Ref.gif` - Original
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5C-Subdiagram.gif) `-Subdiagram.gif` - Original
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CActivityDiagram.png) `ActivityDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CActivityDiagram_folder.png) `ActivityDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CActor.png) `Actor.png` - done
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CActor_GridDiagram.png) `Actor_GridDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CAnalysisDiagram.png) `AnalysisDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CAnalysisDiagram_folder.png) `AnalysisDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CArchiMateDiagram.png) `ArchiMateDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CArchiMateDiagram_folder.png) `ArchiMateDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CBackToTop.gif) `BackToTop.gif`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CBlockDefinitionDiagram.png) `BlockDefinitionDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CBlockDefinitionDiagram_folder.png) `BlockDefinitionDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CBrainstorm.png) `Brainstorm.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CBrainstorm_folder.png) `Brainstorm_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CBusinessMotivationModelDiagram.png) `BusinessMotivationModelDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CBusinessMotivationModelDiagram_folder.png) `BusinessMotivationModelDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CBusinessProcessDiagram.png) `BusinessProcessDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CBusinessProcessDiagram_folder.png) `BusinessProcessDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CBusinessRuleEditorDiagram.png) `BusinessRuleEditorDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CBusinessRule_GridDiagram.png) `BusinessRule_GridDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CChartDiagram.png) `ChartDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CChartDiagram_folder.png) `ChartDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CClassDiagram.png) `ClassDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CClassDiagram_folder.png) `ClassDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5Cclass_model_navigator_tab.png) `class_model_navigator_tab.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CCloseFolder.png) `CloseFolder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CCommunicationDiagram.png) `CommunicationDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CCommunicationDiagram_folder.png) `CommunicationDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CComponentDiagram.png) `ComponentDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CComponentDiagram_folder.png) `ComponentDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CCompositeStructureDiagram.png) `CompositeStructureDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CCompositeStructureDiagram_folder.png) `CompositeStructureDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CConversationDiagram.png) `ConversationDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CConversationDiagram_folder.png) `ConversationDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CCRCCardDiagram.png) `CRCCardDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CCRCCardDiagram_folder.png) `CRCCardDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CDataFlowDiagram.png) `DataFlowDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CDataFlowDiagram_folder.png) `DataFlowDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CDataType.png) `DataType.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CDecisionModelDiagram.png) `DecisionModelDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CDecisionModelDiagram_folder.png) `DecisionModelDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CDeploymentDiagram.png) `DeploymentDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CDeploymentDiagram_folder.png) `DeploymentDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5Cdiagram_navigator_tab.png) `diagram_navigator_tab.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CDocumentation.png) `Documentation.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CDTBDecisionTableEditorDiagram.png) `DTBDecisionTableEditorDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CDTBDecisionTableEditorDiagram_folder.png) `DTBDecisionTableEditorDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CEJBDiagram.png) `EJBDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CEJBDiagram_folder.png) `EJBDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CEPCDiagram.png) `EPCDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CEPCDiagram_folder.png) `EPCDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CERDiagram.png) `ERDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CERDiagram_folder.png) `ERDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CFactDiagram.png) `FactDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CFactDiagram_folder.png) `FactDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CFactTypeEditorDiagram.png) `FactTypeEditorDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CFileReference.png) `FileReference.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CFolderReference.png) `FolderReference.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CFreehandDiagram.png) `FreehandDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CFreehandDiagram_folder.png) `FreehandDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CGlossary_GridDiagram.png) `Glossary_GridDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CGridDiagram.png) `GridDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CGridDiagram_folder.png) `GridDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CImageShape.png) `ImageShape.png` - done
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CInteractionDiagram.png) `InteractionDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CInteractionDiagram_folder.png) `InteractionDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CInteractionOverviewDiagram.png) `InteractionOverviewDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CInteractionOverviewDiagram_folder.png) `InteractionOverviewDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CInternalBlockDiagram.png) `InternalBlockDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CInternalBlockDiagram_folder.png) `InternalBlockDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CLayers.gif) `Layers.gif`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5Clink.png) `link.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5Clogical_view_tab.png) `logical_view_tab.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CMatrixDiagram.png) `MatrixDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CMatrixDiagram_folder.png) `MatrixDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CMindMapDiagram.png) `MindMapDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CMindMapDiagram_folder.png) `MindMapDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CModel.png) `Model.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5Cmodel_navigator_tab.png) `model_navigator_tab.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CObjectDiagram.png) `ObjectDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CObjectDiagram_folder.png) `ObjectDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5COpenFolder.png) `OpenFolder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5COpenSpecification.gif) `OpenSpecification.gif`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5COrganizationChart.png) `OrganizationChart.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5COrganizationChart_folder.png) `OrganizationChart_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CORMDiagram.png) `ORMDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CORMDiagram_folder.png) `ORMDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5COverviewDiagram.png) `OverviewDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5COverviewDiagram_folder.png) `OverviewDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CPackage.png) `Package.png` - done
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CPackageDiagram.png) `PackageDiagram.png` - done
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CPackageDiagram_folder.png) `PackageDiagram_folder.png` - done
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CParametricDiagram.png) `ParametricDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CParametricDiagram_folder.png) `ParametricDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CProcessMapDiagram.png) `ProcessMapDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CProcessMapDiagram_folder.png) `ProcessMapDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CProfileDiagram.png) `ProfileDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CProfileDiagram_folder.png) `ProfileDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5Cproject.png) `project.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CRef.gif) `Ref.gif` - done - actually png
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CReportDiagram.png) `ReportDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CRequirementDiagram.png) `RequirementDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CRequirementDiagram_folder.png) `RequirementDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CRequirement_GridDiagram.png) `Requirement_GridDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CStateDiagram.png) `StateDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CStateDiagram_folder.png) `StateDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CStereotype.png) `Stereotype.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CSubdiagram.gif) `Subdiagram.gif` - done - actually png
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CTermEditorDiagram.png) `TermEditorDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CTextualAnalysis.png) `TextualAnalysis.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CTextualAnalysis_folder.png) `TextualAnalysis_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CTimingDiagram.png) `TimingDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CTimingDiagram_folder.png) `TimingDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CTransitor.gif) `Transitor.gif`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5Ctransp.gif) `transp.gif`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CUrlReference.png) `UrlReference.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CUseCaseDescriptionDiagram.png) `UseCaseDescriptionDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CUseCaseDiagram.png) `UseCaseDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CUseCaseDiagram_folder.png) `UseCaseDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CUseCase_GridDiagram.png) `UseCase_GridDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CUserInterfaceDiagram.png) `UserInterfaceDiagram.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CUserInterfaceDiagram_folder.png) `UserInterfaceDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CWSDLDiagram.png) `WSDLDiagram.png` - done
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CWSDLDiagram_folder.png) `WSDLDiagram_folder.png`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Cimages%5Cicons%5CZachmanDiagram.png) `ZachmanDiagram.png` - done

