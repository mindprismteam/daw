## README

`C:\_\daw\.assets\visual_paradigm_publish\content`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `48`
- `thought_uid` - `C4FF8587-D997-C072-6A63-8D6BF7A094CC`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/smG2FNqGAqAAAhcQ`
- `FmColorName` - `black`
- `ColorHex` - `#525257`
- `ColorRgb` - `82,82,87`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-48.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Ccontent) _Visual Paradigm Source Content Overrides_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%34%37%26webthought_id%3D%34%37%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FfaeKFNqGAqAAAhUt) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Ccontent%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FsmG%32FNqGAqAAAhcQ) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FsmG%32FNqGAqAAAhcQ) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/C%34FF%38%35%38%37-D%39%39%37-C%30%37%32-%36A%36%33-%38D%36BF%37A%30%39%34CC) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/.assets/visual_paradigm_publish/content/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-48) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Ccontent%5Cblank_menu.html) `blank_menu.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Ccontent%5Cdiagram_navigator.html) `diagram_navigator.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Ccontent%5Clink_popup.js) `link_popup.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Ccontent%5Cproject_content.html) `project_content.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Ccontent%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Ccontent%5Creport.css) `report.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Ccontent%5Creport.js) `report.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.assets%5Cvisual_paradigm_publish%5Ccontent%5Ctooltip.js) `tooltip.js`

