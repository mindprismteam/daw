window.DATA={};
window.DATA.windowData={me:{}};

let incX = 0;
let incY = 0;
let scrollIntervalID = -1;
let goToURL = "#";
let shapeSpecURL = "#";
let cursorX = 0;
let cursorY = 0;
window.FIXUPSNEEDED=true;
const BITBUCKET_PRJ='keyprism3';
const BITBUCKET_REPO='keyprism3';
unused(BITBUCKET_PRJ);
unused(BITBUCKET_REPO);
document.documentElement.style.backgroundColor = "black";
function unused(v){
  return v;
}
function cas(e){                                //utils.
  let s = '';
  if (e.ctrlKey) {s += 'c';}
  if (e.altKey) {s += 'a';}
  if (e.shiftKey) {s += 's';}
  return s;
} //-cas


function loadUrl() {
  const lArgString = location.search.substring(1, location.search.length);
  const lArgs = lArgString.split("&");
  for (let i = 0; i < lArgs.length; i++) {
    const lIndex = lArgs[i].indexOf('=');
    if (lIndex>0) {
      const lArgName = lArgs[i].substring(0, lIndex);
      if (lArgName === 'url') {
        //noinspection JSUnresolvedVariable
        parent._content_pane.location.href = lArgs[i].substring(lIndex + 1);
      }
    }
  }
}

unused(initDiagram);
let baseX = 0;
function initDiagram() {
  const diagram = document.getElementById("diagram");
  const spotLight_top = document.getElementById("spotlight_top");
  if (diagram !== null && spotLight_top !== null) {
    baseX = findPosX(diagram)-findPosX(spotLight_top);
  }
  //
  url = window.location.href;
  const shapeIndex = url.indexOf('?shapeid=');
  if (shapeIndex !== -1) {
    const shapeid = url.substring(shapeIndex + '?shapeid='.length);
    const lSelect = document.getElementById("SelectShape");
    if (lSelect !== null) {
      for (let i = 0; i < lSelect.options.length; i++) {
        if (lSelect.options[i].id === "option" + shapeid) {
          lSelect.selectedIndex = i;
          scrollWin(lSelect.value);
          showSpotLight(lSelect.value);
          break;
        }
      }
    }
  }
}
unused(initGridDiagram);
function initGridDiagram() {
  const url = window.location.href;
  const modelIndex = url.indexOf('.html#');
  if (modelIndex !== -1) {
    const modelId = url.substring(modelIndex + '.html#'.length);
    highlightGridModelSelection(modelId);
  }
}

let lastGridModelSelection;
let lastGridModelSelectionBackground;
function highlightGridModelSelection(modelId) {
  if (lastGridModelSelection !== null) {
    lastGridModelSelection.style.background = lastGridModelSelectionBackground;
    lastGridModelSelection = null;
  }
  const lSelect = document.getElementById('modelSelection' + modelId);
  if (lSelect !== null) {
    lastGridModelSelection = lSelect;
    lastGridModelSelectionBackground = lSelect.style.background;
    lSelect.style.background='#FFE0E0';
  }
}

function scrollWin(modelValues) {
  if (modelValues != ''){
    const diagram = document.getElementById("diagram");
    const xOffset = findPosX(diagram);
    const yOffset = findPosY(diagram);
    let viewportWidth = 0, viewportHeight = 0;
    if( typeof( window.innerWidth ) == 'number' ) {
      viewportWidth = window.innerWidth;
      viewportHeight = window.innerHeight;
    } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
      viewportWidth = document.documentElement.clientWidth;
      viewportHeight = document.documentElement.clientHeight;
    } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
      viewportWidth = document.body.clientWidth;
      viewportHeight = document.body.clientHeight;
    }
    const lValues = modelValues.split("|");
    let lValueIndex = 0;
    const shapeOrConnector = lValues[lValueIndex++];
    const shapeX = lValues[lValueIndex++]*1;
    const shapeY = lValues[lValueIndex++]*1;
    const w = lValues[lValueIndex++]*1 + 20;
    const h = lValues[lValueIndex++]*1 + 20;
    const x = shapeX + xOffset*1 - viewportWidth/2 + w/2;
    const y = shapeY + yOffset*1 - viewportHeight/2 + h/2;
    incX = incY = 2;
    if (document.all) {
      if (!document.documentElement.scrollLeft){
        incX *= (document.body.scrollLeft > x?-15:15);
      }else{
        incX *= (document.documentElement.scrollLeft > x?-15:15);
      }
      if (!document.documentElement.scrollTop){
        incY *= (document.body.scrollTop > y?-15:15);
      }else{
        incY *= (document.documentElement.scrollTop > y?-15:15);
      }
    }else{
      incX *= (window.pageXOffset > x?-15:15);
      incY *= (window.pageYOffset > y?-15:15);
    }
    const fn="doScroll(" + x + ", " + y +")";
    scrollIntervalID = setInterval(fn, 1);
    //scrollIntervalID = setInterval("doScroll(" + x + ", " + y +")", 1);
  }
}

unused(doScroll);
function doScroll(x, y) {
  let beforeX;
  let beforeY;
  let afterX;
  let afterY;
  if (document.all){
    if (!document.documentElement.scrollLeft){
      beforeX = document.body.scrollLeft;
    }else{
      beforeX = document.documentElement.scrollLeft;
    }
    if (!document.documentElement.scrollTop){
      beforeY = document.body.scrollTop;
    }else{
      beforeY = document.documentElement.scrollTop;
    }
  }else{
    beforeX = window.pageXOffset;
    beforeY = window.pageYOffset;
  }
  window.scrollTo(beforeX+incX, beforeY+incY);
  if (document.all){
    if (!document.documentElement.scrollLeft){
      afterX = document.body.scrollLeft;
    }else{
      afterX = document.documentElement.scrollLeft;
    }
    if (!document.documentElement.scrollTop){
      afterY = document.body.scrollTop;
    }else{
      afterY = document.documentElement.scrollTop;
    }
  }else{
    afterX = window.pageXOffset;
    afterY = window.pageYOffset;
  }
  if (incX!==0 && beforeX===afterX){
    incX = 0;
  }
  if (incY!==0 && beforeY===afterY){
    incY = 0;
  }
  if ((incX < 0 && (afterX < x || afterX+incX < x)) || (incX > 0 && (afterX > x || afterX+incX > x))) {
    incX = 0;
  } if ((incY < 0 && (afterY < y || afterY+incY < y)) || (incY > 0 && (afterY > y || afterY+incY > y))) {
    incY = 0;
  }
  if (incX===0 && incY===0) {
    window.clearInterval(scrollIntervalID);
  }
}

function findPosX(obj){
  let theLeft = 0;
  if (obj.offsetParent){
    while (obj.offsetParent){
      theLeft += obj.offsetLeft;
      obj = obj.offsetParent;
    }
  }else if (obj.x){
    theLeft += obj.x;
  }
  return theLeft;
}

function findPosY(obj){
  let theTop = 0;
  if (obj.offsetParent){
    while (obj.offsetParent){
      theTop += obj.offsetTop;
      obj = obj.offsetParent;
    }
  }else if (obj.y){
    theTop += obj.y;
  }
  return theTop;
}

function showSpotLight(modelValues) {
  if (modelValues !== '') {
    const diagram = document.getElementById("diagram");
    const xOffset = findPosX(diagram) - baseX;
    const yOffset = findPosY(diagram);
    //noinspection JSAnnotator
    //xOffset -= baseX;
    const lValues = modelValues.split("|");
    let lValueIndex = 0;
    const shapeOrConnector = lValues[lValueIndex++];
    //
    const shapeX = lValues[lValueIndex++]*1;
    const shapeY = lValues[lValueIndex++]*1;
    const x = shapeX + xOffset*1 - 10;
    const y = shapeY + yOffset*1 - 10;
    const w = lValues[lValueIndex++]*1 + 20;
    const h = lValues[lValueIndex++]*1 + 20;
    const url_for_image_map = lValues[lValueIndex++];
    const url_for_spec = lValues[lValueIndex++];
    //
    const spotLight = document.getElementById("spotlight");
    const spotLight_top = document.getElementById("spotlight_top");
    const spotLight_bottom = document.getElementById("spotlight_bottom");
    const spotLight_left = document.getElementById("spotlight_left");
    const spotLight_right = document.getElementById("spotlight_right");
    //
    const spotlight_c = document.getElementById("spotlight_c");
    const spotLightResourcesTop = document.getElementById("spotLightResourcesTop");
    const spotLightResourcesRight = document.getElementById("spotLightResourcesRight");
    const spotLightTable = document.getElementById("spotLightTable");
    const spotLightCell = document.getElementById("spotLightCell");
    //
    if (spotLight === null) {
      spotLight_top.style.width = w+2;
      spotLight_bottom.style.width = w+2;
      spotLight_left.style.height = h+2;
      spotLight_right.style.height = h+2;
      //
      spotLight_top.style.height = 2;
      spotLight_bottom.style.height = 2;
      spotLight_left.style.width = 2;
      spotLight_right.style.width = 2;
    }else{
      spotLight.style.width = w;
      spotLight.style.height = h;
    }
    //
    spotlight_c.width = w;
    spotlight_c.height = h;
    //
    goToURL = url_for_image_map;
    shapeSpecURL = url_for_spec;
    //
    const areaId = lValues[lValueIndex++];
    const areaObj = document.getElementById(areaId);
    if (areaId !== null && spotLightCell !== null) {
      spotLightCell.onmouseover= function onmouseover(event) {
        unused(event);
        if (areaObj !== null) {
          areaObj.onmouseover();
        }
      };
      spotLightCell.onmouseout= function onmouseout(event) {
        unused(event);
        if (areaObj !== null) {
          areaObj.onmouseout();
        }
      };
    }
    //
    const N = (document.all) ? 0 : 1;
    if (N) {
      if (spotLight === null) {
        spotLight_top.style.left = x;
        spotLight_top.style.top = y;
        spotLight_bottom.style.left = x;
        spotLight_bottom.style.top = y+h+2;
        spotLight_left.style.left = x;
        spotLight_left.style.top = y;
        spotLight_right.style.left = x+w;
        spotLight_right.style.top = y;
      }else{
        spotLight.style.left = x;
        spotLight.style.top = y;
        spotLightTable.style.width = w;
        spotLightCell.style.width = w;
        spotLightCell.style.height = h;
      }
      spotlight_c.style.left = x;
      spotlight_c.style.top = y;
      //
      spotLightResourcesTop.style.left = x + w - 50;
      spotLightResourcesTop.style.top = y - 25;
      spotLightResourcesRight.style.left = x + w;
      spotLightResourcesRight.style.top = y + 10;
    }else{
      if (spotLight === null) {
        spotLight_top.style.posLeft = x;
        spotLight_top.style.posTop = y;
        spotLight_bottom.style.posLeft = x;
        spotLight_bottom.style.posTop = y+h;
        spotLight_left.style.posLeft = x;
        spotLight_left.style.posTop = y;
        spotLight_right.style.posLeft = x+w;
        spotLight_right.style.posTop = y;
      }else{
        spotLight.style.posLeft = x;
        spotLight.style.posTop = y;
        spotLightTable.style.width = w;
        spotLightCell.style.width = w;
        spotLightCell.style.height = h;
      }
      spotlight_c.style.posLeft = x;
      spotlight_c.style.posTop = y;
      spotLightResourcesTop.style.posLeft = x + w - 50;
      spotLightResourcesTop.style.posTop = y - 25;
      spotLightResourcesRight.style.posLeft = x + w;
      spotLightResourcesRight.style.posTop = y + 10;
    }
    if (shapeOrConnector === 'connector') {
      // drawing connector
      if (spotLight === null) {
        spotLight_top.style.visibility = "hidden";
        spotLight_bottom.style.visibility = "hidden";
        spotLight_left.style.visibility = "hidden";
        spotLight_right.style.visibility = "hidden";
      }else{
        spotLight.style.visibility = "hidden";
      }
      const lPointCount = lValues[lValueIndex++];
      if (lPointCount > 0) {
        spotlight_c.style.visibility = "visible";
        const context = spotlight_c.getContext('2d');
        {
          context.beginPath();
          for (let lPointIndex = 0; lPointIndex < lPointCount; lPointIndex++) {
            const lPointX = lValues[lValueIndex++]*1 + 10; // +10, because <canvas 's x/y is -10, w/h is + 20.
            const lPointY = lValues[lValueIndex++]*1 + 10;
            if (lPointIndex === 0) {
              context.moveTo(lPointX+0.5, lPointY+0.5);
            }else{
              context.lineTo(lPointX+0.5, lPointY+0.5);
            }
          }
          context.lineWidth = 7;
          context.lineJoin = "round";
          context.strokeStyle="#0000FF";
          context.stroke();
        }
      }else{
        spotlight_c.style.visibility = "hidden"
      }
    }else{
      if (spotLight === null) {
        spotLight_top.style.visibility = "visible";
        spotLight_bottom.style.visibility = "visible";
        spotLight_left.style.visibility = "visible";
        spotLight_right.style.visibility = "visible";
      }else{
        spotLight.style.visibility = "visible";
      }
      spotlight_c.style.visibility = "hidden"
    }
    if (shapeY < 40){
      spotLightResourcesRight.style.visibility = "visible";
      spotLightResourcesTop.style.visibility = "hidden";
    }else{
      spotLightResourcesRight.style.visibility = "hidden";
      spotLightResourcesTop.style.visibility = "visible";
    }
  }else{
    const spotLight = document.getElementById("spotlight");
    const spotLight_top = document.getElementById("spotlight_top");
    const spotLight_bottom = document.getElementById("spotlight_bottom");
    const spotLight_left = document.getElementById("spotlight_left");
    const spotLight_right = document.getElementById("spotlight_right");
    const spotlight_c = document.getElementById("spotlight_c");
    const spotLightResourcesTop = document.getElementById("spotLightResourcesTop");
    const spotLightResourcesRight = document.getElementById("spotLightResourcesRight");
    if (spotLight === null) {
      spotLight_top.style.visibility = "hidden";
      spotLight_bottom.style.visibility = "hidden";
      spotLight_left.style.visibility = "hidden";
      spotLight_right.style.visibility = "hidden";
    }else{
      spotLight.style.visibility = "hidden";
    }
    spotlight_c.style.visibility = "hidden";
    spotLightResourcesTop.style.visibility = "hidden";
    spotLightResourcesRight.style.visibility = "hidden";
  }
  //$('a:eq(0)').focus();
  //window.top.postMessage({from:window.name,action:'focusWindow',windowName:window.name},'*');
  //alert('focus');
  setTimeout(function(){
    //$('body').focus();
    // var i = document.createElement('input');
    // i.style.display = 'none';
    // doc.body.appendChild(i);
    // i.focus();
    // doc.body.removeChild(i);
    //alert('sending');
    //self.focus();
    //$(document).focus();
    //$('body').focus();
    //window.top.postMessage({from:window.name,action:'focusWindow',windowName:window.name},'*');
  },1500);
  //$(window).focus();
}
unused(clearSpotLight);
function clearSpotLight() {
  const spotLight = document.getElementById("spotlight");
  const spotLight_top = document.getElementById("spotlight_top");
  const spotLight_bottom = document.getElementById("spotlight_bottom");
  const spotLight_left = document.getElementById("spotlight_left");
  const spotLight_right = document.getElementById("spotlight_right");
  const spotlight_c = document.getElementById("spotlight_c");
  const spotLightResourcesTop = document.getElementById("spotLightResourcesTop");
  const spotLightResourcesRight = document.getElementById("spotLightResourcesRight");
  if (spotLight === null) {
    spotLight_top.style.visibility = "hidden";
    spotLight_bottom.style.visibility = "hidden";
    spotLight_left.style.visibility = "hidden";
    spotLight_right.style.visibility = "hidden";
  }else{
    spotLight.style.visibility = "hidden";
  }
  spotlight_c.style.visibility = "hidden";
  spotLightResourcesTop.style.visibility = "hidden";
  spotLightResourcesRight.style.visibility = "hidden";
  // document.location.href = goToURL;
}

unused(clearSpotLightFromOpenSpecButton);
function clearSpotLightFromOpenSpecButton() {
  const spotLight = document.getElementById("spotlight");
  const spotLight_top = document.getElementById("spotlight_top");
  const spotLight_bottom = document.getElementById("spotlight_bottom");
  const spotLight_left = document.getElementById("spotlight_left");
  const spotLight_right = document.getElementById("spotlight_right");
  const spotlight_c = document.getElementById("spotlight_c");
  const spotLightResourcesTop = document.getElementById("spotLightResourcesTop");
  const spotLightResourcesRight = document.getElementById("spotLightResourcesRight");
  if (spotLight === null) {
    spotLight_top.style.visibility = "hidden";
    spotLight_bottom.style.visibility = "hidden";
    spotLight_left.style.visibility = "hidden";
    spotLight_right.style.visibility = "hidden";
  }else{
    spotLight.style.visibility = "hidden"
  }
  spotlight_c.style.visibility = "hidden";
  spotLightResourcesTop.style.visibility = "hidden";
  spotLightResourcesRight.style.visibility = "hidden";
  document.location.href = shapeSpecURL;
}
unused(showLayersDialog);
function showLayersDialog(layersDialogId) {
  const layersDialog = document.getElementById(layersDialogId);
  const base = document.getElementById("diagram");
  const xOffset = findPosX(base) + 10;
  const yOffset = findPosY(base);
  layersDialog.style.left = xOffset;
  layersDialog.style.top = yOffset;
  layersDialog.style.display = "block";
  layersDialog.style.zIndex = 5;
}

unused(okLayersDialog);
function okLayersDialog(layersDialogId, checkBoxIds) {
  const layersDialog = document.getElementById(layersDialogId);
  layersDialog.style.display = "none";
  const count = checkBoxIds.length;
  let lHidedCount = 0;
  for (let i = 0; i < count; i++) {
    const checkbox = document.getElementById(checkBoxIds[i]);
    if (checkbox.checked) {
    }else{
      lHidedCount++;
    }
  }
  if (lHidedCount === 0) {
    showComponent("fullDiagram");
    hideComponent("layersBackground");
  }else{
    hideComponent("fullDiagram");
    showComponent("layersBackground");
  }
  for (let i = 0; i < count; i++) {
    const checkbox = document.getElementById(checkBoxIds[i]);
    const rowIds = checkbox.value.split(";");
    if (checkbox.checked) {
      const rowCount = rowIds.length;
      for (let rowIndex = 0; rowIndex < rowCount; rowIndex++) {
        const lRowId = rowIds[rowIndex];
        if (lHidedCount === 0 && lRowId.length > 12 && lRowId.substring(12, 0) === "diagramLayer") {
          hideComponent(lRowId); // hide Diagram, because all layers shown, just show the full diagram
        }else{
          showComponent(lRowId);
        }
      }
    }else{
      const rowCount = rowIds.length;
      for (let rowIndex = 0; rowIndex < rowCount; rowIndex++) {
        hideComponent(rowIds[rowIndex]);
      }
    }
  }
  layersDialog.style.zIndex = 1;
}

function showComponent(componentId) {
  const component = document.getElementById(componentId);
  if (component !== null) {
    if (componentId.substring(6, 0) === "option") {
      component.disabled = false;
    }else if (componentId.substring(4, 0) === "area") {
      component.coords = component.getAttribute("coords2");
    }else if (component.style.display === "none") {
      component.style.display = "";
    }
  }
}

function hideComponent(componentId) {
  const component = document.getElementById(componentId);
  if (component !== null) {
    if (componentId.substring(6, 0) === "option") {
      component.disabled = true;
      const lSelect = document.getElementById("SelectShape");
      if (lSelect !== null) {
        for (let i = 0; i < lSelect.options.length; i++) {
          if (lSelect.options[i].id === componentId && lSelect.selectedIndex === i) {
            lSelect.selectedIndex = 0;
            showSpotLight(lSelect.value);
            break;
          }
        }
      }
    }else if (componentId.substring(4, 0) === "area") {
      component.coords = "0,0,0,0";
    }else if (component.style.display !== "none") {
      component.style.display = "none";
    }
  }
}

unused(collapseLogicalViewElement);
function collapseLogicalViewElement(aLogicalViewElementIds, aExpandAnchorId, aCollapseAnchorId, aOpenFolderImageId, aCloseFolderImageId) {
  const count = aLogicalViewElementIds.length;
  for (let i = 0; i < count; i++) {
    const lLogicalViewElementId = aLogicalViewElementIds[i];
    const lComponentIds = lLogicalViewElementId.split(";"); // [rowId, level] or [rowId, level, expandAnchorId, collapseAnchorId]
    hideComponent(lComponentIds[0]);
  }
  showComponent(aExpandAnchorId);
  showComponent(aCloseFolderImageId);
  hideComponent(aCollapseAnchorId);
  hideComponent(aOpenFolderImageId);
}

unused(expandLogicalViewElement);
function expandLogicalViewElement(aLogicalViewElementIds, aExpandAnchorId, aCollapseAnchorId, aOpenFolderImageId, aCloseFolderImageId) {
  let lCollapsedLevel = -1;
  const count = aLogicalViewElementIds.length;
  for (let i = 0; i < count; i++) {
    const lLogicalViewElementId = aLogicalViewElementIds[i];
    const lComponentIds = lLogicalViewElementId.split(";"); // [rowId, level] or [rowId, level, expandAnchorId, collapseAnchorId]
    const lComponentLevel = lComponentIds[1];
    if (lCollapsedLevel === -1 || lComponentLevel <= lCollapsedLevel) {
      showComponent(lComponentIds[0]);
      lCollapsedLevel = -1;
      if (lComponentIds.length === 4) {
        const lCollapseAnchor = document.getElementById(lComponentIds[2]);
        if (lCollapseAnchor.style.display !== "none") {
          // its children is collapsed, don't expand them
          lCollapsedLevel = lComponentLevel;
        }
      }
    }
  }
  showComponent(aCollapseAnchorId);
  showComponent(aOpenFolderImageId);
  hideComponent(aExpandAnchorId);
  hideComponent(aCloseFolderImageId);
}
unused(updateCursorPos);
function updateCursorPos(event){
  const e = (window.event) ? window.event : event;
  let xOffset = e.clientX;
  let yOffset = e.clientY;
  if (document.all){
    if (!document.documentElement.scrollLeft){
      xOffset += document.body.scrollLeft;
    }else{
      xOffset += document.documentElement.scrollLeft;
    }
    if (!document.documentElement.scrollTop){
      yOffset += document.body.scrollTop;
    }else{
      yOffset += document.documentElement.scrollTop;
    }
  }else{
    xOffset += window.pageXOffset;
    yOffset += window.pageYOffset;
  }
  cursorX = xOffset;
  cursorY = yOffset;
}

unused(selectProject);
function selectProject(aUrl) {
  document.location.href = aUrl;
}

const JavaScript = {
  load: function(src, callback) {
    const script = document.createElement('script');
    let loaded;
    script.setAttribute('src', src);
    if (callback) {
      script.onreadystatechange = script.onload = function() {
        if (!loaded) {
          callback();
        }
        loaded = true;
      };
    }
    document.getElementsByTagName('head')[0].appendChild(script);
  }
  ,addStylesheet:function(url,id){                  //utils.
    const style = document.createElement('link');
    style.rel = 'stylesheet';
    style.type = 'text/css';
    //noinspection JSUnresolvedFunction, JSUnresolvedVariable,ES6ModulesDependencies
    style.href = chrome&&chrome.extension?chrome.extension.getURL(url):url;
    if (id) {
      style.id=id;
    }
    (document.head||document.documentElement).appendChild(style);
  } //-addStylesheet
};

function hideLinkPopup(){//
  const popup = document.getElementById("linkPopupMenuTable");
  if (popup) {
    popup.style.visibility="hidden"
  }
  const lyr=document.getElementById("linkPopupMenuLayer");
  if (lyr){
    lyr.style.visibility="hidden";
  }
}

function searchObj(search){
  return JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
}

function gotoShape(shapeid){
  const lSelect = document.getElementById("SelectShape");
  if (lSelect !== null) {
    for (let i = 0; i < lSelect.options.length; i++) {
      if (lSelect.options[i].id === "option" + shapeid) {
        lSelect.selectedIndex = i;
        scrollWin(lSelect.value);
        hideLinkPopup();
        showSpotLight(lSelect.value);
        break;
      }
    }
  }
}

function locObject(loc){
  if (loc.indexOf('?')===-1) {
    return {location:loc};
  }
  const ro={};
  const a=loc.split('?');
  ro.location=a[0];
  const search=a[1];
  const obj=searchObj(search);
  return $.extend(ro,obj);
}

function locPage(loc){
  const a=loc.split('/');
  return a[a.length-1];
}

function isTop(){
  const n=window.name;
  return n!=='_navigator_pane'&&n!=='_menu_pane'&&n!=='_content_pane';
}



// https://code.jquery.com/jquery-3.2.1.js
// https://code.jquery.com/jquery-3.1.1.slim.min.js

function loadOtherScripts(cb){
  JavaScript.load('https://code.jquery.com/jquery-3.2.1.min.js',function(){
    //console.log('location',location,'window.name',window.name);
    console.log('LOAD jquery',window.name);
    JavaScript.load('../../scripts/jquery.noty.packaged.js',function(){
      console.log('LOAD jquery.noty.packaged',window.name);
      JavaScript.load('../../scripts/chosen.jquery.js',function(){
        JavaScript.addStylesheet('../../scripts/chosen.css','chosen');
        console.log('LOAD chosen.jquery',window.name);
        JavaScript.load('https://use.fontawesome.com/09bc4b9f8f.js',function(){
          console.log('LOAD fontawesome',window.name);
          JavaScript.load('../../scripts/desktopini.js',function(){
            console.log('LOAD desktopini',window.name);
            JavaScript.load('../../scripts/vpmain.js',function(){
              console.log('LOAD vpmain',window.name);
              cb&&cb();
            });
          });
        });
      });
    });
  });
}


function embedYodiz(){
  var s='<div id="yodiz">';
  s+='<iframe';
  s+=' id="yodizIframe"';
  s+=' src="https://app.yodiz.com/plan/pages/board.vz?cid=30887&pid=3"';
  //s+=' style="border:0px;filter:invert(1) hue-rotate(180deg);"';
  s+='></iframe>';
  s+='<div id="yodizTag"></div>';
  s+='</div>';
  $('body').append(s);
}
function embedSmooch(){
  JavaScript.load('https://cdn.smooch.io/smooch.min.js',function(){
    Smooch.init({ appToken: 'as36q6u24ns1xn1uwu984k2o4' });
  });
  function waitFor(){
    var me=$('#sk-messenger-button');
    if (me.length===0) {
      setTimeout(waitFor,200);
      return;
    }
    me.css('background-color','#111111');
  }
  waitFor();

}

function whenScriptsLoaded(){
  console.log('whenScriptsLoaded',window.name);
  $.urlParam = function(name){
    const results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (!results) {
      return;
    }
    return results[1] || 0;
  };
  if ($.urlParam('package')) {
    const pkg=$.urlParam('package');
    const options=$('#SelectShape').find('option');
    console.log('#SelectShape option', options);
    options.each(function(){
      const me=$(this);
      const txt=me.text();
      console.log(txt);
      if (txt===pkg+' : Package'||txt===pkg+' : Model'){
        const val=me.attr('value');
        scrollWin(val);hideLinkPopup();showSpotLight(val);
        return false;
      }
    });
  }
  if (isTop()) {
  }
  if (window.name==='_content_pane') {
    let tgt=$('body>.HeaderText:eq(0)');
    console.log('tgt',tgt);
    tgt.html('');
    tgt
    .css('background-image','url(http://mrobbinsassoc.com/projects/keyprism3/images/logo-work2.png)')
    .css('background-position','left 0%')
    .css('background-size','contain')
    .css('background-repeat','no-repeat')
    .css('min-height','32px')
    .css('margin-top','-14px')
    ;
    const headerText=tgt;
    const tableNext=headerText.next();
    const vplink=$('#vplink');
    const pageTitle=$('.PageTitle');
    const vplinkNextP=pageTitle.prev();
    const pageTitleNextDiv=pageTitle.next();
    const pageTitleNextDivTable=pageTitleNextDiv.find('table:eq(0)');//next();
    const s='<div id="HeaderDiv"></div>';
    headerText.before(s);
    const headerDiv=$('#HeaderDiv');
    //vplinkNextP.css('background-color','red');
    if (1) {
      headerDiv.append(headerText.detach());
      headerDiv.append(tableNext.css('display','none').detach());
      //headerDiv.append(vplink.detach());
      headerDiv.append(vplinkNextP.detach());
      headerDiv.append(pageTitle.css('display','inline-block').detach());
      headerDiv.append(pageTitleNextDivTable.css('position','').css('top','').css('left','').detach());
    }
    const headerDivNext2=headerDiv.next().next();
    headerDivNext2.attr('id','diagramDiv');
    //
    //alert($('#SelectShape option:eq(0)').text());
    $('#SelectShape option:eq(0)').remove();
    //$('#SelectShape').attr('placeholder','Select Model Element');
    $('#SelectShape').chosen({
      placeholder_text_single:'Select Model Element'
      ,search_contains:true
      ,width:300
    });
    $(document).on('keydown',function(evt){
      const cas_=cas(evt);
      if (cas_==='') {
        if (event.keyCode===114) {//f3 ` 192
          $('#SelectShape').trigger('chosen:open');
          return false;
        }
      }
      var foc=$(':focus');
      if (foc.length&&foc.get(0)&&foc.get(0).tagName==='INPUT') {
        if (cas_===''&&event.keyCode===27) { //esc
          foc.blur();
          return false;
        }
        //alert(event.keyCode);
        return;
      }
      // ` 192
      //console.log(evt.keyCode);
      //alert(evt.keyCode);
    });
    embedYodiz();
    embedSmooch();

    //chosen:open
    window.FIXUPSNEEDED=false;
  }else{
    //alert(window.name);
  }
  if (window.name==='_navigator_pane') {
    $('.ProjectName').css('display','none').next().css('display','none');
    $('#header')
    .css('background-image','url(http://mrobbinsassoc.com/projects/keyprism3/images/logo-work2.png)')
    .css('background-position','right 0%')
    .css('background-size','contain')
    .css('background-repeat','no-repeat')
    .css('width','100%');
    let s='';
    s+='<li>';
    s+='  <a target="_menu_pane" href="blank_menu.html">';
    s+='<i class="fa fa-question-circle" style="margin-top:3px;"></i> ';
    //s+='    <center>';
    //s+='      <div style="border: none; width: 18px !important;height: 18px !important;background-image:url(';
    //s+='../images/icons/logical_view_tab.png';
    //s+="       ) !important; background-image:url('');";
    //s+=" filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='../images/icons/logical_view_tab.png');";
    //s+=' margin-top:3px;">';
    //s+='      </div>';
    //s+='    </center>';
    s+='  </a>';
    s+='</li>';
    $('#header>ul').append(s).css('width',null);
    var table=$('#header').next();//.append(s).css('width',null);
    var span=table.find('.TableHeaderText');
    span.after('<input type="text" id="finder" style="font-size:inherit"/>');
    var finder=$('#finder').on('input',function(evt){
      var me=$(this);
      console.log('change',me.val());
      var value=me.val();
      var t2=me.parents('table').next();
      var rows=t2.find('tr');
      rows.each(function(){
        var me=$(this);
        var txt=me.text();
        if (value===''||txt.indexOf(value)!==-1) {
          me.css('display','');
        }else{
          me.css('display','none');
        }
      });
    });
    finder.focus();
    var val=localStorage.getItem('navFinderValue');
    finder.val(val);
    finder.trigger('input');
    $(document).on('keydown',function(evt){
      const cas_=cas(evt);
      const mapnext={
        'diagram_navigator.html':'model_navigator.html'
        ,'model_navigator.html':'class_navigator.html'
        ,'class_navigator.html':'logical_view.html'
        ,'logical_view.html':'diagram_navigator.html'
      }
      const mapprev={
        'diagram_navigator.html':'logical_view.html'
        ,'model_navigator.html':'diagram_navigator.html'
        ,'class_navigator.html':'model_navigator.html'
        ,'logical_view.html':'class_navigator.html'
      }
      if (cas_===''&&evt.keyCode===34) {//pgdn
        const myPage=locPage(location.href);
        var next=mapnext[myPage];
        var val=$('#finder').val();
        localStorage.setItem('navFinderValue',val);
        location.href=next;
        return false;
      }
      if (cas_===''&&evt.keyCode===33) {//pgup
        const myPage=locPage(location.href);
        var prev=mapprev[myPage];
        var val=$('#finder').val();
        localStorage.setItem('navFinderValue',val);
        location.href=prev;
        return false;
      }
      if (cas_===''&&evt.keyCode===112) {//f1p
        window.open('blank_menu.html','_menu_pane');
        return false;
      }
    });
    window.FIXUPSNEEDED=false;
  }
  if (window.name==='_menu_pane') {
    var span=$('.TableHeaderText');
    span.after('<input type="text" id="finder" style="font-size:inherit"/>');
    var finder=$('#finder').on('input',function(evt){
      var me=$(this);
      console.log('change',me.val());
      var value=me.val();
      var t2=me.parents('table').next();
      var rows=t2.find('tr');
      rows.each(function(){
        var me=$(this);
        var txt=me.text();
        if (value===''||txt.indexOf(value)!==-1) {
          me.css('display','');
        }else{
          me.css('display','none');
        }
      });
    });
    finder.focus();
    $(document).on('keydown',function(evt){
      const cas_=cas(evt);
      if (cas_===''&&evt.keyCode===112) {//f1p
        location.href='blank_menu.html';//,'_menu_pane');
        return false;
      }
    });
    window.FIXUPSNEEDED=false;
  }
}


function messagesToTopAction_loaded(event){
  console.log('messagesToTopAction_loaded',event.data.from);
  if (event.data.from==='_menu_pane') {
    var win=window.frames[event.data.from].window;
    var document=win.document;
    //alert('messagesToTopAction_loaded'+(!!document));
    const script = document.createElement('script');
    //alert('messagesToTopAction_loaded'+(!!script));
    script.setAttribute('src', 'report.js');
    win.NEEDSCRIPTS=true;
    document.getElementsByTagName('head')[0].appendChild(script);
  }
  if (event.data.from==='_navigator_pane'||event.data.from==='_content_pane') {
    window.frames[event.data.from].postMessage({from:'Top',action:'completeLoad'},'*');
  }
}
function messagesToTopAction_reportWindowData(event){
  const panes={
    Top:true
    ,_content_pane:true
    ,_brain_pane:true
    ,_menu_pane:true
    ,_navigator_pane:true
  };
  delete panes[event.data.from];
  if (event.data.from!=='Top') {
    delete panes.Top;
    window.DATA.windowData[event.data.from]=event.data.windowData;
  }
  for (let n in panes) {
    window.frames[n].postMessage(event.data,'*');
  }
}

function messagesToTopAction_focusWindow(event){
  window.frames[event.data.windowName].focus();
  setTimeout(function(){
    window.frames[event.data.windowName].focus();
  },500);
}
function messagesToTopAction_rightFrameUpDown(event){
  const content_height=window.frames._content_pane.innerHeight;
  if (!window.DATA.windowData._brain_pane) {
    console.error('!window.DATA.windowData._brain_pane');
  }
  const brain_height=window.DATA.windowData._brain_pane.innerHeight;
  let pct=parseInt(''+((brain_height/(brain_height+content_height))*100));
  console.log('pct',pct);
  if (event.data.action==='rightFrameUp') {
    if (pct<51) {pct=10;}else{pct=50;}
  }else{
    if (pct<49) {pct=50;}else{pct=100;}
  }
  $('#right_frameset').attr('rows',''+pct+'%, *');
  console.log('pct',pct);
}

function messagesToTop(event){
  //console.log('messagesToTop',event.data);
  if (event.data.action==='brainNav') {messagesAction_brainNav(event);}
  if (event.data.action==='reportWindowData') {messagesToTopAction_reportWindowData(event);}
  if (event.data.action==='rightFrameUp'||event.data.action==='rightFrameDown') {
    messagesToTopAction_rightFrameUpDown(event);
  }
  if (event.data.action==='loaded') {
    messagesToTopAction_loaded(event);
  }
  if (event.data.action==='focusWindow') {messagesToTopAction_focusWindow(event);}
  if (event.data.action==='load') {
    if (event.data.target==='_content_pane') {
      if (event.data.url) {
        const msg=event.data;
        window.frames._content_pane.postMessage(msg,'*');
      }
    }
  }
}

function messagesAction_reportWindowData(){
  if (isTop()) {
    return;
  }
  window.DATA.windowData[event.data.from]=event.data.windowData;
}

function messagesToAny(event){
  unused(event);
}

function messagesTo_content_pane(event){
  if (event.data.action==='completeLoad') {
    completeLoad();
  }
  if (event.data.action==='load') {
    const lo=locObject(event.data.url);
    //const keys=Object.keys(lo);
    window.top.postMessage({from:window.name,action:'focusWindow',windowName:window.name},'*');
    if (lo.length===1) {
      console.log('lo.length===1');
      location.href=event.data.url;
    }else{
      const myLocation=locObject(location.href);
      const myPage=locPage(myLocation.location);
      const sentPage=locPage(lo.location);
      console.log('location.href',location.href);
      console.log('myPage',myPage);
      console.log('lo.location',lo.location);
      console.log('sentPage',sentPage);
      if (myPage===sentPage) {
        //noinspection JSUnresolvedVariable
        if (lo.shapeid) {
          //noinspection JSUnresolvedVariable
           console.log('lo.shapeid',lo.shapeid);
           if (window.FIXUPSNEEDED) {
             setTimeout(function(){
               //noinspection JSUnresolvedVariable
               gotoShape(lo.shapeid);
             },1000);
           }else{
             //noinspection JSUnresolvedVariable
             gotoShape(lo.shapeid);
           }
        }
      }else{
        location.href=event.data.url;
      }
    }
  }
}

function messagesTo_navigator_pane(event){
  if (event.data.action==='completeLoad') {
    completeLoad();
  }
}

function messagesTo_menu_pane(event){
  unused(event);
}

function messagesAction_listening(event){
  unused(event);
}

function messagesAction_brainNav(event){
  window.frames._brain_pane.postMessage(event.data,'*');
}

function receiveMessage(event){
  //
  //debugger;
  // if (event.data.action==='brainNav') {
  //   alert(JSON.stringify(event.data.action));
  //   window.frames._brain_pane.postMessage(event.data,'*');
  // }
  //console.log('receiveMessage:'+window.name,event.data);
  if (isTop()) {messagesToTop(event);}
  if (event.data.action==='reportWindowData') {messagesAction_reportWindowData(event);}
  if (event.data.action==='listening') {messagesAction_listening(event);}
  //
  if (event.data.action!=='reportWindowData') {
    event.data.action&&console.log('window.receiveMessage:location',location,event.data);
  }
  if (window.name==='_content_pane') {messagesTo_content_pane(event);}
  if (window.name==='_navigator_pane') {messagesTo_navigator_pane(event);}
  if (window.name==='_menu_pane') {messagesTo_menu_pane(event);}
  //
  if (event.data.from==='WebBrain') {
    if (event.data.action==='heightUpdate') {
      window.brain_height=event.data.innerHeight;
    }
    if (event.data.action==='rightFrameUp'||event.data.action==='rightFrameDown') {
      //document.getElementsByTagName()
    }
  }
}

window.addEventListener("message", receiveMessage, false);

let doWindowData=true;
if (doWindowData) {
  if (true||window!==top) {
    setInterval(function(){
      if (window.$) {
        if (window.DATA.windowData.me.innerWidth!==window.innerWidth
          ||window.DATA.windowData.me.innerHeight!==window.innerHeight
          ||window.DATA.windowData.me.locationHref!==''+window.location.href
          ) {
          window.DATA.windowData.me.innerWidth=window.innerWidth;
          window.DATA.windowData.me.innerHeight=window.innerHeight;
          window.DATA.windowData.me.locationHref=''+window.location.href;
          const msg={
            locationHref:''+location.href
            ,action:'reportWindowData'
            ,windowData:{
              locationHref:''+location.href
              ,innerWidth:window.innerWidth
              ,innerHeight:window.innerHeight
            }
          };
          if (isTop()) {
            msg.from='Top';
          }else{
            msg.from=window.name;
          }
          top.postMessage(msg,'*');
        }
      }
    },1000);
  }
}

function completeLoad(){
  console.log('completeLoad',window.name);
  loadOtherScripts(whenScriptsLoaded);
}

if (window.NEEDSCRIPTS) {
  delete window.NEEDSCRIPTS;
  completeLoad();
}
