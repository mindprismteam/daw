## RMF-.scrap-eslint

`C:\_\daw\.scrap\eslint`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `62`
- `thought_uid` - `14F04891-55B6-3D69-E7D7-10CD2D91ECF4`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/kJc5FNqGAqAAAhj5`
- `FmColorName` - `yellow-dark`
- `ColorHex` - `#E6F940`
- `ColorRgb` - `230,249,64`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-dark-48.png)](aip://open/C%3A%5C_%5Cdaw%5C.scrap%5Ceslint) _Stash for EsLint Confiigs_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.scrap%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%35%39%26webthought_id%3D%35%39%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FMxPr%35NqGAqAAAgz%30) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.scrap%5Ceslint%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FkJc%35FNqGAqAAAhj%35) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FkJc%35FNqGAqAAAhj%35) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%31%34F%30%34%38%39%31-%35%35B%36-%33D%36%39-E%37D%37-%31%30CD%32D%39%31ECF%34) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/.scrap/eslint/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-62) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.scrap%5Ceslint%5Cfull%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%36%33%26webthought_id%3D%36%33%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FxQ%36%35FNqGAqAAAhkJ)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.scrap%5Ceslint%5Cfull%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%36%33%26webthought_id%3D%36%33%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FxQ%36%35FNqGAqAAAhkJ) `full` - EsLint RC File
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.scrap%5Ceslint%5Cother%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%36%34%26webthought_id%3D%36%34%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2Fkiu%35FNqGAqAAAhkV)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.scrap%5Ceslint%5Cother%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%36%34%26webthought_id%3D%36%34%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2Fkiu%35FNqGAqAAAhkV) `other` - EsLint RC file

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_json-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.scrap%5Ceslint%5Ceslintrc.json) `eslintrc.json`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.scrap%5Ceslint%5CREADME.html) `README.html`

