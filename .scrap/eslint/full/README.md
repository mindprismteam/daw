## README

`C:\_\daw\.scrap\eslint\full`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `63`
- `thought_uid` - `2D8465CB-69B3-3541-48CD-F60F74C66FA5`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/xQ65FNqGAqAAAhkJ`
- `FmColorName` - `yellow`
- `ColorHex` - `#F2FE7B`
- `ColorRgb` - `242,254,123`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-48.png)](aip://open/C%3A%5C_%5Cdaw%5C.scrap%5Ceslint%5Cfull) _EsLint RC File_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.scrap%5Ceslint%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%36%32%26webthought_id%3D%36%32%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FkJc%35FNqGAqAAAhj%35) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.scrap%5Ceslint%5Cfull%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FxQ%36%35FNqGAqAAAhkJ) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FxQ%36%35FNqGAqAAAhkJ) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%32D%38%34%36%35CB-%36%39B%33-%33%35%34%31-%34%38CD-F%36%30F%37%34C%36%36FA%35) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/.scrap/eslint/full/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-63) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_eslintrc-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.scrap%5Ceslint%5Cfull%5C.eslintrc) `.eslintrc` - Settings
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.scrap%5Ceslint%5Cfull%5CREADME.html) `README.html`

