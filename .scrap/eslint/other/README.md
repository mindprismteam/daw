## README

`C:\_\daw\.scrap\eslint\other`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `64`
- `thought_uid` - `750FCD4A-41DD-2760-80EA-BA009610B4D2`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/kiu5FNqGAqAAAhkV`
- `FmColorName` - `yellow`
- `ColorHex` - `#F2FE7B`
- `ColorRgb` - `242,254,123`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-48.png)](aip://open/C%3A%5C_%5Cdaw%5C.scrap%5Ceslint%5Cother) _EsLint RC file_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.scrap%5Ceslint%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%36%32%26webthought_id%3D%36%32%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FkJc%35FNqGAqAAAhj%35) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.scrap%5Ceslint%5Cother%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2Fkiu%35FNqGAqAAAhkV) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2Fkiu%35FNqGAqAAAhkV) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%37%35%30FCD%34A-%34%31DD-%32%37%36%30-%38%30EA-BA%30%30%39%36%31%30B%34D%32) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/.scrap/eslint/other/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-64) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_eslintrc-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.scrap%5Ceslint%5Cother%5C.eslintrc) `.eslintrc` - Settings
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.scrap%5Ceslint%5Cother%5CREADME.html) `README.html`

