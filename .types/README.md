## README

`C:\_\daw\.types`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `66`
- `thought_uid` - `33C5EB88-6325-C6BA-5B52-B0D500390FB6`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/Y9Pr5NqGAqAAAg0C`
- `FmColorName` - `blue-dark`
- `ColorHex` - `#455DAC`
- `ColorRgb` - `69,93,172`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-blue-dark-48.png)](aip://open/C%3A%5C_%5Cdaw%5C.types) _Types Local to project for Webstorm_

Files in the folder contain no-ops that help Webstorm in object recognition. Example:

    Meteor.call = function(name, profileId, user, args) {};

`.types` are project based, `.webstorm` are long-running.

The success of this is undetermined, see also `.webstorm`.

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%32%26webthought_id%3D%32%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FE%31%36L%35NqGAqAAAgwr) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.types%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FY%39Pr%35NqGAqAAAg%30C) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FY%39Pr%35NqGAqAAAg%30C) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%33%33C%35EB%38%38-%36%33%32%35-C%36BA-%35B%35%32-B%30D%35%30%30%33%39%30FB%36) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/.types/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-66) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

- `<types.js>`

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.types%5CREADME.html) `README.html`

