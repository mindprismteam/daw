## RMF-_extra-vp-vpworkspace-.seprefdata

`C:\_\daw\_extra\vp\vpworkspace\.seprefdata`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `76`
- `thought_uid` - `61015E65-14F1-22A2-F0CD-F95C422ADA97`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/YCLtFNqGAqAAAhoY`
- `FmColorName` - `red-light`
- `ColorHex` - `#FDA79B`
- `ColorRgb` - `253,167,155`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-48.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.seprefdata) _Visual Paradigm Application Preferences_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%37%35%26webthought_id%3D%37%35%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FZLutFNqGAqAAAhoG) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.seprefdata%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FYCLtFNqGAqAAAhoY) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FYCLtFNqGAqAAAhoY) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%36%31%30%31%35E%36%35-%31%34F%31-%32%32A%32-F%30CD-F%39%35C%34%32%32ADA%39%37) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/_extra/vp/vpworkspace/.seprefdata/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-76) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.seprefdata%5C.layout%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%37%37%26webthought_id%3D%37%37%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FgUYdFNqGAqAAAhoq)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.seprefdata%5C.layout%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%37%37%26webthought_id%3D%37%37%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FgUYdFNqGAqAAAhoq) `.layout` - Visual Paradigm Application Layout Preferences

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.seprefdata%5C.colorFormat) `.colorFormat`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.seprefdata%5C.genericShapeStencil) `.genericShapeStencil`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.seprefdata%5C.perspective) `.perspective`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.seprefdata%5C.recentOpenFiles) `.recentOpenFiles`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.seprefdata%5C.shadowFormat) `.shadowFormat`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.seprefdata%5CREADME.html) `README.html`

