## README

`C:\_\daw\_extra\vp\vpworkspace`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `75`
- `thought_uid` - `FE288242-0BD1-5203-EE90-4C2E671985A3`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/ZLutFNqGAqAAAhoG`
- `FmColorName` - `red`
- `ColorHex` - `#F56C4B`
- `ColorRgb` - `245,108,75`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-48.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace) _Visual Paradigm Workspace Folder_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%37%34%26webthought_id%3D%37%34%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2F%30j%34tFNqGAqAAAhnw) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FZLutFNqGAqAAAhoG) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FZLutFNqGAqAAAhoG) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/FE%32%38%38%32%34%32-%30BD%31-%35%32%30%33-EE%39%30-%34C%32E%36%37%31%39%38%35A%33) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/_extra/vp/vpworkspace/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-75) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

- `<other>.vpp`

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.seprefdata%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%37%36%26webthought_id%3D%37%36%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FYCLtFNqGAqAAAhoY)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.seprefdata%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%37%36%26webthought_id%3D%37%36%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FYCLtFNqGAqAAAhoY) `.seprefdata` - Visual Paradigm Application Preferences
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%37%38%26webthought_id%3D%37%38%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FOrJdFNqGAqAAAhpD)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%37%38%26webthought_id%3D%37%38%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FOrJdFNqGAqAAAhpD) `.vpprefdata` - Visual Paradigm Project Preferences
- ![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png) `images` - Git Ignored
- ![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png) `src` - Git Ignored
- ![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png) `tmp` - Git Ignored
- ![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png) `voices` - Git Ignored

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.project) `.project`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5Cdaw.vpp) `daw.vpp`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5CteamworkUser.xml) `teamworkUser.xml`

