## RMF-_extra-vp-vpworkspace-.vpprefdata-dictionary

`C:\_\daw\_extra\vp\vpworkspace\.vpprefdata\dictionary`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `79`
- `thought_uid` - `2F83A24B-0AA6-DB0F-BA6A-804CD824E7A0`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/y9ndFNqGAqAAAhpa`
- `FmColorName` - `red-light`
- `ColorHex` - `#FDA79B`
- `ColorRgb` - `253,167,155`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-48.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5Cdictionary) _Visual Paradigm Dictionary Files_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%37%38%26webthought_id%3D%37%38%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FOrJdFNqGAqAAAhpD) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5Cdictionary%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2Fy%39ndFNqGAqAAAhpa) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2Fy%39ndFNqGAqAAAhpa) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%32F%38%33A%32%34B-%30AA%36-DB%30F-BA%36A-%38%30%34CD%38%32%34E%37A%30) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/_extra/vp/vpworkspace/.vpprefdata/dictionary/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-79) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5Cdictionary%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5Cdictionary%5Cuser.lx) `user.lx` - Text, unknown format, need to explore

