## RMF-_extra-vp-vpworkspace-.vpprefdata-LayoutPerspectiveData

`C:\_\daw\_extra\vp\vpworkspace\.vpprefdata\LayoutPerspectiveData`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `80`
- `thought_uid` - `9E593982-2DE6-689E-0BF1-B69E1362A368`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/pTs9FNqGAqAAAhps`
- `FmColorName` - `black`
- `ColorHex` - `#525257`
- `ColorRgb` - `82,82,87`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-48.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CLayoutPerspectiveData) _Visual Paradigm Layout Data_

`*.jpeg` - Git Ignored

`*.lod` - Git Ignored

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%37%38%26webthought_id%3D%37%38%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FOrJdFNqGAqAAAhpD) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CLayoutPerspectiveData%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FpTs%39FNqGAqAAAhps) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FpTs%39FNqGAqAAAhps) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%39E%35%39%33%39%38%32-%32DE%36-%36%38%39E-%30BF%31-B%36%39E%31%33%36%32A%33%36%38) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/_extra/vp/vpworkspace/.vpprefdata/LayoutPerspectiveData/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-80) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CLayoutPerspectiveData%5CREADME.html) `README.html`

