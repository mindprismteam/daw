## README

`C:\_\daw\_extra\vp\vpworkspace\.vpprefdata`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `78`
- `thought_uid` - `2463E5D3-9E48-82C7-5E9C-19E6C8737DD3`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/OrJdFNqGAqAAAhpD`
- `FmColorName` - `red-light`
- `ColorHex` - `#FDA79B`
- `ColorRgb` - `253,167,155`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-48.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata) _Visual Paradigm Project Preferences_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%37%35%26webthought_id%3D%37%35%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FZLutFNqGAqAAAhoG) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FOrJdFNqGAqAAAhpD) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FOrJdFNqGAqAAAhpD) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%32%34%36%33E%35D%33-%39E%34%38-%38%32C%37-%35E%39C-%31%39E%36C%38%37%33%37DD%33) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/_extra/vp/vpworkspace/.vpprefdata/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-78) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

- Unknown

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5Cdictionary%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%37%39%26webthought_id%3D%37%39%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2Fy%39ndFNqGAqAAAhpa)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5Cdictionary%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%37%39%26webthought_id%3D%37%39%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2Fy%39ndFNqGAqAAAhpa) `dictionary` - Visual Paradigm Dictionary Files
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CLayoutPerspectiveData%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%38%30%26webthought_id%3D%38%30%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FpTs%39FNqGAqAAAhps)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CLayoutPerspectiveData%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%38%30%26webthought_id%3D%38%30%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FpTs%39FNqGAqAAAhps) `LayoutPerspectiveData` - Visual Paradigm Layout Data
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%38%32%26webthought_id%3D%38%32%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FBae%39FNqGAqAAAhqA)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%38%32%26webthought_id%3D%38%32%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FBae%39FNqGAqAAAhqA) `modelTypePreferData` - Visual Paradigm Model Type Defaults
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5Cnicknames%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%38%33%26webthought_id%3D%38%33%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2F.on%39FNqGAqAAAhqS)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5Cnicknames%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%38%33%26webthought_id%3D%38%33%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2F.on%39FNqGAqAAAhqS) `nicknames` - Visual Paradigm Nicknames
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CprojectsViewInfo%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%38%34%26webthought_id%3D%38%34%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2F%33loDFNqGAqAAAhqf)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CprojectsViewInfo%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%38%34%26webthought_id%3D%38%34%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2F%33loDFNqGAqAAAhqf) `projectsViewInfo` - Visual Paradigm DiagramOpenInfos
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5Ctreestates%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%38%35%26webthought_id%3D%38%35%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2Fz_%36DFNqGAqAAAhq%31)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5Ctreestates%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%38%35%26webthought_id%3D%38%35%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2Fz_%36DFNqGAqAAAhq%31) `treestates` - Visual Paradigm Treestate Data

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5C.autoSaveProject) `.autoSaveProject` - Git Ignored
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5C.autoSaveProjectInfo) `.autoSaveProjectInfo` - Git Ignored
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5C.systemproject.vpp) `.systemproject.vpp` - Binary, SQLite
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5C.vp.preference) `.vp.preference` - Project Preferences Xml
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CREADME.html) `README.html`

