## README

`C:\_\daw\_extra\vp\vpworkspace\.vpprefdata\modelTypePreferData`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `82`
- `thought_uid` - `B9DB7E47-6D22-3B6B-B075-E639134CAC95`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/Bae9FNqGAqAAAhqA`
- `FmColorName` - `red-light`
- `ColorHex` - `#FDA79B`
- `ColorRgb` - `253,167,155`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-48.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData) _Visual Paradigm Model Type Defaults_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-light-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%37%38%26webthought_id%3D%37%38%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FOrJdFNqGAqAAAhpD) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FBae%39FNqGAqAAAhqA) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FBae%39FNqGAqAAAhqA) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/B%39DB%37E%34%37-%36D%32%32-%33B%36B-B%30%37%35-E%36%33%39%31%33%34CAC%39%35) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/_extra/vp/vpworkspace/.vpprefdata/modelTypePreferData/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-82) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

- Unknown

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CActor) `Actor` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CArtifact) `Artifact` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CBMMStrategy) `BMMStrategy` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CBMMTactic) `BMMTactic` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CBusinessRule) `BusinessRule` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CBusinessRuleGroup) `BusinessRuleGroup` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CClass) `Class` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CComponent) `Component` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CDiagramOverview) `DiagramOverview` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CFactType) `FactType` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CGlossary) `Glossary` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CInstanceSpecification) `InstanceSpecification` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CModel) `Model` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CNode) `Node` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CPackage) `Package` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CRectangle) `Rectangle` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CSubsystem) `Subsystem` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CUILabel) `UILabel` - Text, unstudied format
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5Cvp%5Cvpworkspace%5C.vpprefdata%5CmodelTypePreferData%5CUITabbedHeader) `UITabbedHeader` - Text, unstudied format

