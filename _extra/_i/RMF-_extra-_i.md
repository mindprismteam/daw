## RMF-_extra-_i

`C:\_\daw\_extra\_i`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `73`
- `thought_uid` - `9A5B3C27-4D88-C705-6B5F-0B6F63F2D732`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/mUFNFNqGAqAAAhnH`
- `FmColorName` - `yellow-light`
- `ColorHex` - `#F5FE9E`
- `ColorRgb` - `245,254,158`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-light-48.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5C_i) _Text notes driven by `!i.txt`_

Also ties into `.scrap/_extra/_i/`

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_extra%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%37%32%26webthought_id%3D%37%32%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FR_Pr%35NqGAqAAAg%30X) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5C_i%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FmUFNFNqGAqAAAhnH) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FmUFNFNqGAqAAAhnH) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%39A%35B%33C%32%37-%34D%38%38-C%37%30%35-%36B%35F-%30B%36F%36%33F%32D%37%33%32) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/_extra/_i/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-73) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_extra%5C_i%5CREADME.html) `README.html`

