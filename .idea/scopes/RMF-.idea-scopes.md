## RMF-.idea-scopes

`C:\_\daw\.idea\scopes`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `58`
- `thought_uid` - `7F93C788-F3ED-21E8-961C-8AF63DA8C295`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/EODxFNqGAqAAAhhS`
- `FmColorName` - `gray`
- `ColorHex` - `#9999A2`
- `ColorRgb` - `153,153,162`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-48.png)](aip://open/C%3A%5C_%5Cdaw%5C.idea%5Cscopes) _Webstorm Scopes_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5C.idea%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%35%31%26webthought_id%3D%35%31%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2F%31.Pr%35NqGAqAAAgzt) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.idea%5Cscopes%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FEODxFNqGAqAAAhhS) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FEODxFNqGAqAAAhhS) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%37F%39%33C%37%38%38-F%33ED-%32%31E%38-%39%36%31C-%38AF%36%33DA%38C%32%39%35) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/.idea/scopes/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-58) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

- `<other-scopes.xml>`

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C.idea%5Cscopes%5CREADME.html) `README.html`

