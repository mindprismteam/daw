## README

`C:\_\daw\_docs\procedures`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `69`
- `thought_uid` - `6F302723-8DAB-4822-9FDE-941DECFA5089`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/It3VFNqGAqAAAhlu`
- `FmColorName` - `yellow`
- `ColorHex` - `#F2FE7B`
- `ColorRgb` - `242,254,123`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-48.png)](aip://open/C%3A%5C_%5Cdaw%5C_docs%5Cprocedures) _Markdowns For Procedure Instructions_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_docs%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%36%38%26webthought_id%3D%36%38%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2F%35PPr%35NqGAqAAAg%30Q) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_docs%5Cprocedures%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FIt%33VFNqGAqAAAhlu) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FIt%33VFNqGAqAAAhlu) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%36F%33%30%32%37%32%33-%38DAB-%34%38%32%32-%39FDE-%39%34%31DECFA%35%30%38%39) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/_docs/procedures/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-69) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

- `<procedure>.md`

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_docs%5Cprocedures%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_docs%5Cprocedures%5CREADME.md) `README.md`
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_docs%5Cprocedures%5CRMF-_docs-procedures.md) `RMF-_docs-procedures.md`

