## README

`C:\_\daw\_docs`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `68`
- `thought_uid` - `A8231A37-53EC-D438-6892-CFC21DC6EA16`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/5PPr5NqGAqAAAg0Q`
- `FmColorName` - `yellow-dark`
- `ColorHex` - `#E6F940`
- `ColorRgb` - `230,249,64`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-dark-48.png)](aip://open/C%3A%5C_%5Cdaw%5C_docs) _Project Readmes_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%32%26webthought_id%3D%32%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FE%31%36L%35NqGAqAAAgwr) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_docs%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2F%35PPr%35NqGAqAAAg%30Q) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2F%35PPr%35NqGAqAAAg%30Q) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/A%38%32%33%31A%33%37-%35%33EC-D%34%33%38-%36%38%39%32-CFC%32%31DC%36EA%31%36) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/_docs/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-68) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

- `<platform>.md`
- `<aspect>.md`

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_docs%5Cprocedures%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%36%39%26webthought_id%3D%36%39%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FIt%33VFNqGAqAAAhlu)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_docs%5Cprocedures%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%36%39%26webthought_id%3D%36%39%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FIt%33VFNqGAqAAAhlu) `procedures` - Markdowns For Procedure Instructions
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_docs%5Csnapshots%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%37%30%26webthought_id%3D%37%30%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FtmQ%31FNqGAqAAAhl%37)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_docs%5Csnapshots%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%37%30%26webthought_id%3D%37%30%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FtmQ%31FNqGAqAAAhl%37) `snapshots` - Screenshot `png`s
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-yellow-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_docs%5Cstages%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%37%31%26webthought_id%3D%37%31%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FaqU%31FNqGAqAAAhmL)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5C_docs%5Cstages%5CDesktop.ini%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%37%31%26webthought_id%3D%37%31%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FaqU%31FNqGAqAAAhmL) `stages` - Like `proceedures` but at project startup and config

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_docs%5Cconventions.md) `conventions.md` - Tree Categorized Convention Lists
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_docs%5Cfacts.md) `facts.md` - List in order of Discovery
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_docs%5Clog.md) `log.md` - Zdot List Description
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_docs%5Cobserve.md) `observe.md` - List - not quite `seems`, which are more important witnessings, observe is more general and less critical
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_docs%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_docs%5CREADME.md) `README.md`
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_docs%5CRMF-_docs.md) `RMF-_docs.md`
- [![](http://mrobbinsassoc.com/images/icons/md/_md-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_docs%5Cseems.md) `seems.md` - Tree Categorized Lists - Not Quite Facts Yet

