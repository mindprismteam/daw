## RMF-app-components

`C:\_\daw\app\components`

- `FmColorName` - `cyan`
- `ColorHex` - `#78C9FC`
- `ColorRgb` - `120,201,252`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-48.png)](aip://open/C%3A%5C_%5Cdaw%5Capp%5Ccomponents) _React Components_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-purple-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5Capp%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%38%38%26webthought_id%3D%38%38%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FOyvr%35NqGAqAAAg%30s) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5Capp%5Ccomponents%5CDesktop.ini) - ini

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/app/components/) - bitbucket

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5Capp%5Ccomponents%5CREADME.html) `README.html`

