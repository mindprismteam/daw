## RMF-app

`C:\_\daw\app`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `88`
- `thought_uid` - `75A46ECF-E5F7-5964-BAD5-B6EFE9BA36D6`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/Oyvr5NqGAqAAAg0s`
- `FmColorName` - `purple-dark`
- `ColorHex` - `#BE67D5`
- `ColorRgb` - `190,103,213`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-purple-dark-48.png)](aip://open/C%3A%5C_%5Cdaw%5Capp) _Main Application_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%32%26webthought_id%3D%32%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FE%31%36L%35NqGAqAAAgwr) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5Capp%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FOyvr%35NqGAqAAAg%30s) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FOyvr%35NqGAqAAAg%30s) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%37%35A%34%36ECF-E%35F%37-%35%39%36%34-BAD%35-B%36EFE%39BA%33%36D%36) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/app/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-88) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-cyan-16.png)](aip://open/C%3A%5C_%5Cdaw%5Capp%5Ccomponents%5CDesktop.ini)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5Capp%5Ccomponents%5CDesktop.ini) `components` - React Components
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-green-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5Capp%5Cimages%5CDesktop.ini)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Cdaw%5Capp%5Cimages%5CDesktop.ini) `images` - Served Images

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5Capp%5Cindex.html) `index.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5Capp%5Cmain.css) `main.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_jsx-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5Capp%5Cmain.jsx) `main.jsx`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5Capp%5CREADME.html) `README.html`

