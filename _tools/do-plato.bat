cd %~dp0

mkdir .\..\._plato\_

REM DO NOT DELETE HISTORY!

REM SET D=assets
REM CALL :KILLDIR
REM SET D=files
REM CALL :KILLDIR

rem del .\..\._plato\index.html
rem del .\..\._plato\report.


rem assets
rem assets\css
rem assets\css\vendor
rem assets\font
rem assets\scripts
rem assets\scripts\bundles
rem assets\scripts\vendor
rem assets\scripts\vendor\codemirror
rem assets\scripts\vendor\codemirror\util
rem files





cd ..\._plato

SET D=assets
CALL :KEEPDIR
SET D=assets\css
CALL :KEEPDIR
SET D=assets\css\vendor
CALL :KEEPDIR
SET D=assets\font
CALL :KEEPDIR
SET D=assets\scripts
CALL :KEEPDIR
SET D=assets\scripts\bundles
CALL :KEEPDIR
SET D=assets\scripts\vendor
CALL :KEEPDIR
SET D=assets\scripts\vendor\codemirror
CALL :KEEPDIR
SET D=assets\scripts\vendor\codemirror\util
CALL :KEEPDIR
SET D=files
CALL :KEEPDIR

cd ..

call _tools\do-plato-pt1.bat
echo done
cd %~dp0
cd ..

COPY .assets\plato\assets\css\*.css ._plato\assets\css /y
COPY .assets\plato\assets\css\vendor\*.css ._plato\assets\css\vendor /y
COPY .assets\plato\assets\font\*.eot ._plato\assets\font /y
COPY .assets\plato\assets\font\*.svg ._plato\assets\font /y
COPY .assets\plato\assets\font\*.ttf ._plato\assets\font /y
COPY .assets\plato\assets\font\*.woff ._plato\assets\font /y
COPY .assets\plato\assets\scripts\*.js ._plato\assets\scripts /y
COPY .assets\plato\assets\scripts\bundles\*.js ._plato\assets\scripts\bundles /y
COPY .assets\plato\assets\scripts\vendor\*.js ._plato\assets\scripts\vendor /y
COPY .assets\plato\assets\scripts\vendor\codemirror\*.js ._plato\assets\scripts\vendor\codemirror /y
COPY .assets\plato\assets\scripts\vendor\codemirror\util\*.js ._plato\assets\scripts\vendor\codemirror\util /y
COPY .assets\plato\assets\scripts\vendor\codemirror\util\*.css ._plato\assets\scripts\vendor\codemirror\util /y


cd ._plato


SET D=assets
CALL :EXPLDIR
SET D=assets\css
CALL :EXPLDIR
SET D=assets\css\vendor
CALL :EXPLDIR
SET D=assets\font
CALL :EXPLDIR
SET D=assets\scripts
CALL :EXPLDIR
SET D=assets\scripts\bundles
CALL :EXPLDIR
SET D=assets\scripts\vendor
CALL :EXPLDIR
SET D=assets\scripts\vendor\codemirror
CALL :EXPLDIR
SET D=assets\scripts\vendor\codemirror\util
CALL :EXPLDIR
SET D=files
CALL :EXPLDIR

del /f /s /q _ 1>nul
rmdir /s /q _
rmdir /s /q _


REM dir
REM GOTO :EOF
git add -u
git add .
git commit -am auto
git push

cd ..
GOTO :EOF
rem start powershell -c (New-Object Media.SoundPlayer "C:\Windows\Media\notify.wav").PlaySync();

:KEEPDIR
MKDIR _\%D%

ATTRIB -s -h %D%\Desktop.ini
COPY %D%\Desktop.ini _\%D%
ATTRIB +s +h %D%\Desktop.ini

COPY %D%\*.$explhere _\%D%
COPY %D%\*.md _\%D%

ATTRIB -s -h %D%\FolderMarker.ico
COPY %D%\FolderMarker.ico _\%D%
ATTRIB +s +h %D%\FolderMarker.ico

COPY %D%\*.url _\%D%
EXIT /B


:EXPLDIR
IF NOT EXIST %D% EXIT /B

ATTRIB -s -h .\%D%\Desktop.ini
COPY .\_\%D%\Desktop.ini .\%D% /y
ATTRIB +s +h .\%D%\Desktop.ini

ATTRIB -s -h .\%D%\FolderMarker.ico
COPY .\_\%D%\FolderMarker.ico .\%D% /y
ATTRIB +s +h .\%D%\FolderMarker.ico

COPY .\_\%D%\*.$explhere .\%D% /y
COPY .\_\%D%\*.md .\%D% /y
COPY .\_\%D%\*.url .\%D% /y
ATTRIB +r %D%

EXIT /B



:EOF

