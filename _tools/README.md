## README

`C:\_\daw\_tools`

- `brain_uid` - `B531C917-1880-8E60-9CF1-CA5C3BDD5DCC`
- `thought_id` - `86`
- `thought_uid` - `4858DEB2-C10C-B67F-F5F1-7AF27528292F`
- `vp_url` - `daw.vpp://shape/h_KL5NqGAqAAAgwh/Wwvr5NqGAqAAAg0e`
- `FmColorName` - `red-dark`
- `ColorHex` - `#F43F2A`
- `ColorRgb` - `244,63,42`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-48.png)](aip://open/C%3A%5C_%5Cdaw%5C_tools) _Various Batch Tools_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Cdaw%5CREADME.md%3Fbrain_uid%3DB%35%33%31C%39%31%37-%31%38%38%30-%38E%36%30-%39CF%31-CA%35C%33BDD%35DCC%26thought_id%3D%32%26webthought_id%3D%32%26vp_url%3Ddaw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FE%31%36L%35NqGAqAAAgwr) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_tools%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FWwvr%35NqGAqAAAg%30e) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/daw.vpp%3A%2F%2Fshape%2Fh_KL%35NqGAqAAAgwh%2FWwvr%35NqGAqAAAg%30e) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%34%38%35%38DEB%32-C%31%30C-B%36%37F-F%35F%31-%37AF%32%37%35%32%38%32%39%32F) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/mindprismteam/daw/src/master/_tools/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/B531C917-1880-8E60-9CF1-CA5C3BDD5DCC#-86) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/daw/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/daw/vp/publish/content/PackageDiagram_h_KL5NqGAqAAAgwh.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_tools%5Caip.au%33) `aip.au3`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_tools%5Caip.reg) `aip.reg`
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_tools%5Cdeploy.bat) `deploy.bat` - Deploy to Galaxy - obsolete
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_tools%5Cdo-esdoc-pt%31.bat) `do-esdoc-pt1.bat` - Helper
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_tools%5Cdo-esdoc.bat) `do-esdoc.bat` - Create EsDoc Report
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_tools%5Cdo-plato-pt%31.bat) `do-plato-pt1.bat` - Helper
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_tools%5Cdo-plato-style-rev.bat) `do-plato-style-rev.bat`
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_tools%5Cdo-plato-style.bat) `do-plato-style.bat`
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_tools%5Cdo-plato.bat) `do-plato.bat` - Create Plato Report
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_tools%5Cdo-vp-ftp-update.bat) `do-vp-ftp-update.bat`
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_tools%5Cdo-vp-publish-rev-style.bat) `do-vp-publish-rev-style.bat` - Copy VP styles from published to assets
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_tools%5Cdo-vp-publish-style.bat) `do-vp-publish-style.bat` - Helper to `do-vp-publish.bat`
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_tools%5Cdo-vp-publish-upload.bat) `do-vp-publish-upload.bat` - Helper to `do-vp-publish.bat`
- [![](http://mrobbinsassoc.com/images/icons/md/_bat-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_tools%5Cdo-vp-publish.bat) `do-vp-publish.bat` - Create Visual Paradigm Publish, after generated from VP
- [![](http://mrobbinsassoc.com/images/icons/md/_json-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_tools%5Cesdoc.json) `esdoc.json` - EsDoc Configuration
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_tools%5Creadme-folder-ini.au%33) `readme-folder-ini.au3`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Cdaw%5C_tools%5CREADME.html) `README.html`

